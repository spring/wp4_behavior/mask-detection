#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import os
import pathlib
import sys
import warnings

import cv2
import numpy as np
import ros_numpy
import rospy
from hri_msgs.msg import IdsList, NormalizedRegionOfInterest2D
from sensor_msgs.msg import CompressedImage, Image
from wp4_msgs.msg import FaceMask

file_path = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(file_path))
sys.path.append(str(file_path.parent.parent))

from utils import (
    decode_bbox,
    generate_anchors,
    load_pytorch_model,
    pytorch_inference,
    single_class_non_max_suppression,
)

warnings.filterwarnings("ignore")

MAX_ROIS_DISTANCE = 200.0
MAX_SCALING_ROIS = 5.0


class MaskDetector:
    def __init__(self):
        """Initialize ros publisher, ros subscriber"""
        self.model_path = os.path.join(file_path, "models/model360.pth")
        self.model = load_pytorch_model(self.model_path)

        # anchor configuration
        feature_map_sizes = [[45, 45], [23, 23], [12, 12], [6, 6], [4, 4]]
        anchor_sizes = [
            [0.04, 0.056],
            [0.08, 0.11],
            [0.16, 0.22],
            [0.32, 0.45],
            [0.64, 0.72],
        ]
        anchor_ratios = [[1, 0.62, 0.42]] * 5
        anchors = generate_anchors(feature_map_sizes, anchor_sizes, anchor_ratios)

        # for inference , the batch size is 1, the model output shape is [1, N, 4],
        # so we expand dim for anchors to [1, anchor_num, 4]
        self.anchors_exp = np.expand_dims(anchors, axis=0)

        self.detect_every = 5
        self.no_img = 0
        self.mask_id = 0

        self.image_subscriber = rospy.Subscriber(
            rospy.get_param("~input_topic"),
            CompressedImage,
            self._callback,
        )

        self.faces_tracked_pub = rospy.Publisher(
            "/humans/faces/tracked",
            IdsList,
            queue_size=10,
            latch=True,
        )
        self.tracked_faces = []

        self.topic_name_roi = lambda x: f"/humans/faces/{str(x).zfill(6)}/roi"
        self.topic_name_has_mask = lambda x: f"/humans/faces/{str(x).zfill(6)}/has_mask"
        self.topic_name_cropped = lambda x: f"/humans/faces/{str(x).zfill(6)}/cropped"

        self.past_pub = {}

    def _callback(self, ros_img):
        """callback function for mask detection"""
        self.no_img += 1

        np_arr = np.frombuffer(ros_img.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

        if self.no_img % self.detect_every != 0:
            return

        mask_bboxes, _ = self.inference(image_np, target_shape=(360, 360))
        new_idxs = []
        new_bboxes = []
        for mask_bbox in mask_bboxes:
            has_mask, conf, bbox = bool(mask_bbox[0]), mask_bbox[1], mask_bbox[2:]

            # Create the messages.
            timestamp = ros_img.header.stamp
            # width = bbox[2] - bbox[0]
            # height = bbox[3] - bbox[1]
            # roi_msg = RegionOfInterest2D(bbox[0], bbox[1], width, height, False)
            roi_msg = NormalizedRegionOfInterest2D()
            roi_msg.header.stamp = timestamp
            roi_msg.xmin = bbox[0] / image_np.shape[1]
            roi_msg.ymin = bbox[1] / image_np.shape[0]
            roi_msg.xmax = bbox[2] / image_np.shape[1]
            roi_msg.ymax = bbox[3] / image_np.shape[0]
            roi_msg.c = conf

            has_mask_msg = FaceMask()
            has_mask_msg.header.stamp = timestamp
            has_mask_msg.is_wearing_mask = has_mask
            img_crop = image_np[bbox[1] : bbox[3], bbox[0] : bbox[2]][..., ::-1]

            cropped_msg = ros_numpy.msgify(Image, img_crop, encoding="rgb8")
            cropped_msg.header.stamp = timestamp

            # Match face with past detection.
            idx = self.find_previous_match(roi_msg)
            old_pub = self.past_pub.get(idx)
            if idx is not None and old_pub is not None:
                # Retrieve the old topics.
                roi_pub = old_pub["roi"]
                has_mask_pub = old_pub["has_mask"]
                cropped_pub = old_pub["cropped"]
            # Generate a new tracked face.
            else:
                idx = self.mask_id
                self.mask_id += 1

                # Create the new topics.
                roi_pub = rospy.Publisher(
                    self.topic_name_roi(idx),
                    NormalizedRegionOfInterest2D,
                    latch=True,
                )
                has_mask_pub = rospy.Publisher(
                    self.topic_name_has_mask(idx),
                    FaceMask,
                    latch=True,
                )
                cropped_pub = rospy.Publisher(
                    self.topic_name_cropped(idx),
                    Image,
                    latch=True,
                )

                # Register the new topics.
                self.past_pub[idx] = {}
                self.past_pub[idx]["roi"] = roi_pub
                self.past_pub[idx]["has_mask"] = has_mask_pub
                self.past_pub[idx]["cropped"] = cropped_pub

            # Publish the messages.
            roi_pub.publish(roi_msg)
            has_mask_pub.publish(has_mask_msg)
            cropped_pub.publish(cropped_msg)

            new_idxs.append(idx)
            new_bboxes.append(roi_msg)

        # Publish new tracked idxs.
        if len(new_idxs) > 0:
            face_idxs = IdsList(ros_img.header, [str(idx).zfill(6) for idx in new_idxs])
            self.faces_tracked_pub.publish(face_idxs)
            self.tracked_faces = list(zip(new_idxs, new_bboxes))

        # Unregister publishers with no new message.
        # ? This means that the face is not in the scene anymore.
        filtered_pub = {}
        for key, value in self.past_pub.items():
            if key in new_idxs:
                filtered_pub[key] = value
            else:
                value["roi"].unregister()
                value["has_mask"].unregister()
                value["cropped"].unregister()
        self.past_pub = filtered_pub

    def distance_rois(self, bbox_1, bbox_2):
        # x1, y1 = bbox_1.x_offset + bbox_1.width / 2, bbox_1.y_offset + bbox_1.height / 2
        # x2, y2 = bbox_2.x_offset + bbox_2.width / 2, bbox_2.y_offset + bbox_2.height / 2
        x1, y1 = (bbox_1.xmin + bbox_1.xmax) / 2, (bbox_1.ymin + bbox_1.ymax) / 2
        x2, y2 = (bbox_2.xmin + bbox_2.xmax) / 2, (bbox_2.ymin + bbox_2.ymax) / 2

        return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)

    def find_previous_match(self, bbox):
        for idx, prev_bbox in self.tracked_faces:
            distance = self.distance_rois(prev_bbox, bbox)
            print(distance)
            is_short_distance = distance < MAX_ROIS_DISTANCE * MAX_ROIS_DISTANCE
            is_width_scale_ok = (
                # 1 / MAX_SCALING_ROIS < prev_bbox.width / bbox.width < MAX_SCALING_ROIS
                1 / MAX_SCALING_ROIS
                < (prev_bbox.xmax - prev_bbox.xmin) / (bbox.xmax - bbox.xmin)
                < MAX_SCALING_ROIS
            )
            is_height_scale_ok = (
                # 1 / MAX_SCALING_ROIS < prev_bbox.height / bbox.height < MAX_SCALING_ROIS
                1 / MAX_SCALING_ROIS
                < (prev_bbox.ymax - prev_bbox.ymin) / (bbox.ymax - bbox.ymin)
                < MAX_SCALING_ROIS
            )
            if is_short_distance and is_width_scale_ok and is_height_scale_ok:
                return idx
        return None

    def inference(
        self,
        image,
        conf_thresh=0.5,
        iou_thresh=0.4,
        target_shape=(160, 160),
    ):
        """
        Main function of detection inference
        :param image: 3D numpy array of image
        :param conf_thresh: the min threshold of classification probabity.
        :param iou_thresh: the IOU threshold of NMS
        :param target_shape: the model input size.
        :return:
        """
        output_info = []
        height, width, _ = image.shape
        image_resized = cv2.resize(image, target_shape)
        image_np = image_resized / 255.0
        image_exp = np.expand_dims(image_np, axis=0)

        image_transposed = image_exp.transpose((0, 3, 1, 2))
        y_bboxes_output, y_cls_output = pytorch_inference(self.model, image_transposed)

        # remove the batch dimension, for batch is always 1 for inference.
        y_bboxes = decode_bbox(self.anchors_exp, y_bboxes_output)[0]
        y_cls = y_cls_output[0]

        # To speed up, do single class NMS, not multiple classes NMS.
        bbox_max_scores = np.max(y_cls, axis=1)
        bbox_max_score_classes = np.argmax(y_cls, axis=1)

        # keep_idx is the alive bounding box after nms.
        keep_idxs = single_class_non_max_suppression(
            y_bboxes,
            bbox_max_scores,
            conf_thresh=conf_thresh,
            iou_thresh=iou_thresh,
        )
        for idx in keep_idxs:
            conf = float(bbox_max_scores[idx])
            class_id = bbox_max_score_classes[idx]
            bbox = y_bboxes[idx]

            # clip the coordinate, avoid the value exceed the image boundary.
            xmin = max(0, int(bbox[0] * width))
            ymin = max(0, int(bbox[1] * height))
            xmax = min(int(bbox[2] * width), width)
            ymax = min(int(bbox[3] * height), height)

            output_info.append([class_id, conf, xmin, ymin, xmax, ymax])

        return [output_info, image]


if __name__ == "__main__":
    rospy.init_node("mask_detector", anonymous=True)
    detector = MaskDetector()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down ROS Image feature detector module")
