# syntax=docker/dockerfile:1

FROM nvcr.io/nvidia/cuda:11.0.3-cudnn8-devel-ubuntu20.04 AS builder
SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime
RUN echo "Installing Python and Pytorch..." && \
    apt-get update --fix-missing && \
    apt-get install -q -y --no-install-recommends \
        python3-dev \
        python3-pip \
        tzdata && \
    pip3 install torch torchvision --extra-index-url https://download.pytorch.org/whl/cu113 && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /workspace
RUN echo "Installing ROS Noetic..." && \
    apt-get update --fix-missing && \
    apt-get install -q -y --no-install-recommends \
        curl && \
    echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
        /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
        apt-key add - && \
    apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends \
        ros-noetic-ros-core && \
    rm -rf /var/lib/apt/lists/* && \
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc

FROM builder as requirements
SHELL ["/bin/bash", "-c"]
RUN echo "Installing required packages..." && \
    apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends \
        ros-noetic-ros-numpy \
        # ros-noetic-hri-msgs \
        ros-noetic-vision-opencv \
        git
RUN echo "Installing hri_msgs from source..." && \
    . /opt/ros/noetic/setup.bash && \
    mkdir -p /home/spring_ws/src && \
    cd /home/spring_ws/src && \
    git clone https://github.com/ros4hri/hri_msgs.git && \
    cd hri_msgs && \
    # https://github.com/ros4hri/hri_msgs/tree/e38e187010e16ed4328c3135e8c9e83e3f10dd96
    git checkout e38e187010e16ed4328c3135e8c9e83e3f10dd96 && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/ros/noetic .. && \
    make && \
    make install

FROM requirements AS compiler
SHELL ["/bin/bash", "-c"]
COPY ./entrypoint.sh ./entrypoint.sh
COPY ./ /home/spring_ws/src/wp4_mask_detection
COPY ./wp4_msgs /home/spring_ws/src/wp4_msgs
RUN echo "Compiling packages..." && \
    chmod +x ./entrypoint.sh && \
    . /opt/ros/noetic/setup.bash && cd /home/spring_ws && catkin_make && \
    echo "source /home/spring_ws/devel/setup.bash" >> ~/.bashrc
EXPOSE 11311
ENTRYPOINT ["/entrypoint.sh"]
CMD ["run"]
