# Mask Detection



## Docker

### Build

```sh
docker build --target compiler --tag wp4_mask_detection:TAG .
```

### Run

```sh
# Default run
docker run --rm -it --net host --env ROS_MASTER_URI=http://ari-Xc:11311 --gpus '"device=0"' wp4_mask_detection:TAG

# Run with basestation
docker run --rm -it --net host --env ROS_MASTER_URI=http://ari-Xc:11311 --gpus '"device=0"' wp4_mask_detection:TAG basestation
```

## ROS Topics

### Dependencies

 - Head front camera RGB images (from ARI or basestation)

### Published

- /humans/faces/{ids}/roi
- /humans/faces/{ids}/cropped
- /humans/faces/{ids}/has_mask
- /humans/faces/tracked
