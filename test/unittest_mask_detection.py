#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pathlib
import sys

file_path = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(file_path))
sys.path.append(str(file_path.parent.parent))
print(sys.path)

import unittest

# ROS libraries
import roslib
import rospy
from time import sleep

# Ros Messages
from sensor_msgs.msg import CompressedImage
from wp4_msgs.msg import BoxDetection, BoxDetectionFrame


class TestMaskDetection(unittest.TestCase):

    mask_detection_ok = False

    def _callback(self, data):
        self.mask_detection_ok = True

    def test_if_mask_detection_publish(self):
        rospy.Subscriber("/vision_msgs/mask_detections", BoxDetectionFrame, self._callback)

        counter = 0
        while not rospy.is_shutdown() and counter < 5 and (not self.mask_detection_ok):
            sleep(1)
            counter += 1

        self.assertTrue(self.mask_detection_ok)
        
    
if __name__ == "__main__":
    import rostest
    rospy.init_node("test_mask_detection", log_level=rospy.INFO)
    rostest.rosrun("mask-detection", 'unittest_mask_detection', TestMaskDetection)
